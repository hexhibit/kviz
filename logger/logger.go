package log

import (
	"os"
	"github.com/op/go-logging"
)

var Log = logging.MustGetLogger("kviz")


// Example format string. Everything except the message has a custom color
// which is dependent on the log level. Many fields have a custom output
// formatting too, eg. the time returns the hour down to the milli second.
var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
)

func ConfigureLogger(level logging.Level) {

	// For demo purposes, create two backend for os.Stderr.
	backend1 := logging.NewLogBackend(os.Stdout, "", 0)

	// Only errors and more severe messages should be sent to backend1
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(level, "")

	logging.SetFormatter(format)

	Log.SetBackend(backend1Leveled)

}

func Logger() (logger *logging.Logger) {
	return Log
}