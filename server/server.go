package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"time"

	"github.com/araddon/dateparse"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/hexhibit/kviz/controller"
	"gitlab.com/hexhibit/kviz/logger"
	"os"
)

func Run(controller *controller.Controller) {
	r := mux.NewRouter()

	//websocket handler
	r.HandleFunc("/ws/notify", func(writer http.ResponseWriter, request *http.Request) {
		handleNotifier(controller, writer, request)
	})

	//get cluster with at specific time
	logger := log.Logger()

	r.HandleFunc("/get", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "application/json")

		timeParam := request.URL.Query().Get("time")

		var data []byte
		var err error

		if timeParam == "" {
			data, err = controller.ListGraph(time.Now())
		} else {

			t, err := dateparse.ParseAny(timeParam)
			if err != nil {
				logger.Error(err)
				writer.WriteHeader(http.StatusInternalServerError)
				return
			}
			data, err = controller.ListGraph(t)
		}

		if err != nil {
			logger.Error(err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		writer.Write(data)
	})

	//Update cluster. returns delta changes between a specified time range (from / to)
	r.HandleFunc("/update", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "application/json")

		fromParam := request.URL.Query().Get("from")
		toParam := request.URL.Query().Get("to")
		if fromParam == "" || toParam == "" {
			writer.WriteHeader(http.StatusBadRequest)
			return
		} else {
			from, err := dateparse.ParseAny(fromParam)
			to, err := dateparse.ParseAny(toParam)

			if from.After(to) {
				writer.WriteHeader(http.StatusBadRequest)
				return
			}

			if err != nil {
				logger.Error(err)
				writer.WriteHeader(http.StatusInternalServerError)
				return
			}

			data, err := controller.GetDelta(from, to)
			if err != nil {
				logger.Error(err)
				writer.WriteHeader(http.StatusInternalServerError)
				return
			}

			writer.Write(data)
		}
	})

	//Update cluster. returns delta changes between a specified time range (from / to)
	r.HandleFunc("/spec", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "application/json")

		id := request.URL.Query().Get("id")
		if id == "" {
			writer.WriteHeader(http.StatusBadRequest)
			return
		} else {
			logger.Debugf("query node id: %s", id)
			data, err := controller.GetNodeInfo(id)
			if err != nil {
				logger.Error(err)
				writer.WriteHeader(http.StatusInternalServerError)
				return
			}

			writer.Write(data)
		}
	})

	//get a complete ordered list with event timestamps
	r.HandleFunc("/events", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "application/json")

		eventTimesMap := make(map[int64]bool)

		for _, t := range controller.Events {
			eventTimesMap[t.Unix()] = true
		}

		eventTimes := make([]int64, 0)
		for k := range eventTimesMap {
			eventTimes = append(eventTimes, k)
		}

		sort.Slice(eventTimes, func(i, j int) bool {
			return eventTimes[i] < eventTimes[j]
		})

		data, err := json.Marshal(eventTimes)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		writer.Write(data)
	})

	r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("static/"))))

	logger.Infof("Start server on 80")
	corsObj := handlers.AllowedOrigins([]string{"*"})

	loggedRouter := handlers.LoggingHandler(os.Stdout, handlers.CORS(corsObj)(r))
	logger.Fatal(http.ListenAndServe(":80", loggedRouter))

}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
} // use default options

func handleNotifier(controller *controller.Controller, w http.ResponseWriter, r *http.Request) {

	//Upgrade connection: https://de.wikipedia.org/wiki/WebSocket#Antwort_des_Servers
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Logger().Errorf("write: %v", err)
		return
	}

	defer c.Close()

	log.Logger().Info("start ws connection")
	//websocket event loop
	for {
		//blocking receive
		msg := <-controller.EventChannel
		err = c.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%d", msg.Unix())))
		if err != nil {
			log.Logger().Errorf("write: %v", err)
			break
		}
	}
}
