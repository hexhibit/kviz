package graph

import (
	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
	"fmt"
	"os"
	"k8s.io/api/core/v1"
	logger "gitlab.com/hexhibit/kviz/logger"
	"strings"
	"github.com/johnnadratowski/golang-neo4j-bolt-driver/structures/graph"
	"time"
	"github.com/go-errors/errors"
)

var log = logger.Logger()

type Graph struct {
	Connection bolt.Conn
}

var (
	Pool bolt.DriverPool
)

//Throws panic if failing
func InitGraphDb() {

	username := os.Getenv("NEO4J_USERNAME")
	password := os.Getenv("NEO4J_PASSWORD")
	host := os.Getenv("NEO4J_HOST")
	port := os.Getenv("NEO4J_PORT")

	url := ""
	if username == "" && password == "" {

		url = fmt.Sprintf("bolt://%s:%s", host, port)
	} else {
		url = fmt.Sprintf("bolt://%s:%s@%s:%s", username, password, host, port)
	}

	logUrl := strings.Replace(url, password, "*****", 1)
	log.Infof("Opening connection on: %s", logUrl)

	driverPool, err := bolt.NewDriverPool(url, 256)
	if err != nil {
		panic("could not open neo4j db connection on " + strings.Replace(url, password, "*******", 1) + ": " + err.Error())
	}
	log.Infof("connection open")

	Pool = driverPool
}

//Open neo4j database connection. Read username, password and url from env
func GetGraph() (*Graph, error) {
	con, err := Pool.OpenPool()
	if err != nil {
		return nil, err
	}
	return &Graph{Connection: con}, nil
}

func (g *Graph) ClearDatabase() {

	stmt := "MATCH(n)" +
		"DETACH DELETE n;"

	_, err := g.Connection.ExecNeo(stmt, nil)
	if err != nil {
		log.Errorf("could not clear database: " + err.Error())
	}

}

//Create nodes
func (g *Graph) CreateNodes(nodes []*v1.Node) {

	for _, n := range nodes {

		properties := "{name: {name}, created: {created}}"

		smt := fmt.Sprintf("CREATE (n:Node:`%s` %s)", n.Name, properties)
		_, err := g.Connection.ExecNeo(smt, map[string]interface{}{"name": n.Name, "created": n.CreationTimestamp.Unix()})
		if err != nil {
			log.Errorf("could not create node: %v", err)
		}

	}

}

//Create pods with container and relationships.
func (g *Graph) CreatePods(pods []*v1.Pod) {

	for _, p := range pods {

		params := make(map[string]interface{})
		labelPlaceholder := ""
		for lk, lv := range p.Labels {
			labelPlaceholder += fmt.Sprintf("`%s`: {`%s`}, ", lk, lk)
			params[lk] = lv
		}

		properties := "{%s name: {name}, namespace: {namespace}, created: {created}, phase: {phase}}"
		params["name"] = p.Name
		params["namespace"] = p.Namespace
		params["created"] = p.CreationTimestamp.Unix()
		params["phase"] = string(p.Status.Phase)

		properties = fmt.Sprintf(properties, labelPlaceholder)

		smt := fmt.Sprintf("CREATE (p:Pod:`%s` %s)", p.Name, properties)
		_, err := g.Connection.ExecNeo(smt, params)
		if err != nil {
			log.Errorf("error: " + err.Error() + ", " + p.Name)
		}

		//Ignore error
		g.createContainer(p)

	}

}

func (g *Graph) createContainer(p *v1.Pod) error {
	//create container
	containerStmts := make([]string, 0)
	insertProperties := make([]map[string]interface{}, 0)
	for _, c := range p.Spec.Containers {

		stmt := "CREATE (c:Container:`%s` %s) " +
			"WITH c " +
			"MATCH(p:Pod:`%s`) " +
			"CREATE (c)-[r:DEPLOYED_ON {created: {created}}]->(p)"

		properties := "{name: {name}, namespace: {namespace}, created: {created}, state: {state}}"
		params := make(map[string]interface{})
		params["name"] = c.Name
		params["namespace"] = p.Namespace
		params["created"] = p.CreationTimestamp.Unix()
		params["state"] = getContainerStatus(p.Status.ContainerStatuses, c)

		stmt = fmt.Sprintf(stmt, c.Name, properties, p.Name)
		containerStmts = append(containerStmts, stmt)

		insertProperties = append(insertProperties, params)
	}
	_, err := g.Connection.ExecPipeline(containerStmts, insertProperties...)
	if err != nil {
		log.Errorf("could not create container relations: %v", err)
		return err
	}
	return nil
}

func getContainerStatus(cStates []v1.ContainerStatus, container v1.Container) string {
	for _, cs := range cStates {
		if cs.Name == container.Name {

			if cs.State.Running != nil {
				return "running"
			}

			if cs.State.Terminated != nil {
				return "terminated"
			}

			//Default is waiting
			return "waiting"

		}
	}
	return ""
}

func (g *Graph) CreateNodeToNodeRel(newNode *v1.Node, existingNodes map[string]*v1.Node) {

	for _, node := range existingNodes {
		insertStmt := `MATCH (n1:Node:%s),(n2:Node:%s)
CREATE (n1)-[r:CONNECTED {created: {created}}]->(n2)
CREATE (n2)-[r1:CONNECTED {created: {created}}]->(n1)
RETURN type(r)`

		var relCreationTime int64 = 0

		if newNode.CreationTimestamp.Time.Before(node.CreationTimestamp.Time) {
			relCreationTime = node.CreationTimestamp.Unix()
		} else {
			relCreationTime = newNode.CreationTimestamp.Unix()
		}

		insertStmt = fmt.Sprintf(insertStmt, node.Name, newNode.Name)
		_, err := g.Connection.ExecNeo(insertStmt, map[string]interface{}{"created": relCreationTime})
		if err != nil {
			log.Errorf("error: " + err.Error())
		}
	}

}

func (g *Graph) CreatePodToNodeRel(nodes []*v1.Node, pods []*v1.Pod) {

	log.Debugf("Create new pod to node relationship")

	insertStmt := "MATCH (n:Node:%s),(p:Pod:`%s`)" +
		"CREATE (p)-[r:RUN_ON {created: {created}}]->(n)" +
		"RETURN type(r)"

	for _, p := range pods {
		relCreated := false

		podNodeName := p.Spec.NodeName

		for _, n := range nodes {
			if n.Name == podNodeName {
				stmt := fmt.Sprintf(insertStmt, n.Name, p.Name)
				_, err := g.Connection.ExecNeo(stmt, map[string]interface{}{"created": p.CreationTimestamp.Unix()})
				relCreated = true
				if err != nil {
					log.Errorf("error: " + err.Error())
				}
				break
			}
		}

		if !relCreated {
			log.Errorf("could not create any relationship for pod %v", p)
		}

	}

}

func (g *Graph) DeletePod(pod *v1.Pod) {
	stmt := "MATCH(p:Pod:`%s`) SET p.deleted = {deleted}"
	stmt = fmt.Sprintf(stmt, pod.Name)

	insertProperties := make(map[string]interface{}, 0)
	insertProperties["deleted"] = pod.DeletionTimestamp.Unix()

	_, err := g.Connection.ExecNeo(stmt, insertProperties)
	if err != nil {
		log.Errorf("error: " + err.Error())
	}

	//TODO mark relationships as deleted

}

func (g *Graph) DeleteNode(node *v1.Pod) {
	stmt := "MATCH(n:Node:`%s`) SET n.deleted = {deleted}"
	stmt = fmt.Sprintf(stmt, node.Name)

	insertProperties := make(map[string]interface{}, 0)
	insertProperties["deleted"] = node.DeletionTimestamp.Unix()

	_, err := g.Connection.ExecNeo(stmt, insertProperties)
	if err != nil {
		log.Errorf("error: " + err.Error())
	}

	//TODO mark relationships as deleted

}

type KvisNode struct {
	Id        string `json:"id"`
	NodeGroup string    `json:"group"`
	Name      string
	Namespace string
	State     string `json:"state"`
}

type KvisLink struct {
	Source      string `json:"source"`
	Destination string `json:"target"`
	Value       int    `json:"value"`
}

func (g *Graph) List(time time.Time) ([]KvisNode, []KvisLink, error) {


	log.Debugf("Query for: %d", time.Unix())

	stmt := fmt.Sprintf("MATCH (n) WHERE (n.deleted > %d OR n.deleted IS NULL) AND (n.created < %d OR n.created IS NULL) RETURN n", time.Unix(), time.Unix())

	data, _, _, err := g.Connection.QueryNeoAll(stmt, nil)

	if err != nil {
		log.Errorf("could not execute node query: %v", err)
		return nil, nil, err
	}


	nodes := getNodes(data)

	log.Debugf("Found total %d nodes", len(nodes))

	links := make([]KvisLink, 0)

	query := fmt.Sprintf("MATCH (n)-[r]->() WHERE (r.deleted > %d OR r.deleted IS NULL) AND (r.created < %d OR r.created IS NULL) RETURN r", time.Unix(), time.Unix())
	dataLinks, _, _, err := g.Connection.QueryNeoAll(query, nil)

	if err != nil {
		log.Errorf("could not execute link query: %v", err)
		return nil, nil, err
	}

	for _, row := range dataLinks {
		rel := row[0].(graph.Relationship)

		src := fmt.Sprintf("%d", rel.StartNodeIdentity)
		dst := fmt.Sprintf("%d", rel.EndNodeIdentity)

		links = append(links, KvisLink{Source: src, Destination: dst, Value: 1})

	}

	log.Debugf("Found total %d links", len(links))

	return nodes, links, nil
}

func createLinks(data [][]interface{}) []KvisLink {
	links := make([]KvisLink, 0)
	for _, row := range data {
		rel := row[0].(graph.Relationship)

		src := fmt.Sprintf("%d", rel.StartNodeIdentity)
		dst := fmt.Sprintf("%d", rel.EndNodeIdentity)

		links = append(links, KvisLink{Source: src, Destination: dst, Value: 1})

	}
	return links
}

func getNodes(data [][]interface{}) []KvisNode {
	nodes := make([]KvisNode, 0)
	for _, row := range data {
		node := row[0].(graph.Node)
		nodeType := node.Labels[0]

		state := ""

		switch nodeType {
		case "Pod":
			state = node.Properties["phase"].(string)
		case "Container":
			state = node.Properties["state"].(string)
		}

		id := fmt.Sprintf("%d", node.NodeIdentity)

		nodeName := node.Properties["name"].(string)

		//kubernetes nodes don't have a namespace!
		nodeNamespace, ok := node.Properties["namespace"]
		if ok {
			nodeNamespace = nodeNamespace.(string)
		} else {
			nodeNamespace = ""
		}

		kvisNode := KvisNode{Id: id, NodeGroup: nodeType, Name: nodeName, Namespace: nodeNamespace.(string), State: state}
		nodes = append(nodes, kvisNode)

	}
	return nodes
}

func (g *Graph) ListDelta(from time.Time, to time.Time) ([]KvisNode, []KvisNode, []KvisLink, []KvisLink, error) {

	log.Debugf("Query for range : %d to %d", from.Unix(), to.Unix())

	createsNodeQuery := fmt.Sprintf("MATCH (n) WHERE (n.created > %d) AND (n.created <= %d) RETURN n", from.Unix(), to.Unix())
	deletedNodeQuery := fmt.Sprintf("MATCH (n) WHERE (n.deleted > %d) AND (n.deleted <= %d) RETURN n", from.Unix(), to.Unix())

	createdNodeData, _, _, err := g.Connection.QueryNeoAll(createsNodeQuery, nil)
	deletedNodeData, _, _, err := g.Connection.QueryNeoAll(deletedNodeQuery, nil)

	if err != nil {
		log.Errorf("could not execute node createdQuery: %v", err)
		return nil, nil, nil, nil, err
	}

	createdNodes := make([]KvisNode, 0)
	createdNodes = getNodes(createdNodeData)

	deletedNodes := make([]KvisNode, 0)
	deletedNodes = getNodes(deletedNodeData)

	log.Debugf("Found total %d created Nodes", len(createdNodes))
	log.Debugf("Found total %d deleted Nodes", len(deletedNodes))

	createdQuery := fmt.Sprintf("MATCH (n)-[r]->() WHERE (r.created > %d) AND (r.created <= %d) RETURN r", from.Unix(), to.Unix())
	deletedLinkQuery := fmt.Sprintf("MATCH (n)-[r]->() WHERE (r.deleted > %d) AND (r.deleted <= %d) RETURN r", from.Unix(), to.Unix())

	dataLinks, _, _, err := g.Connection.QueryNeoAll(createdQuery, nil)
	if err != nil {
		log.Errorf("could not execute link deleted Query: %v", err)
		return nil, nil, nil, nil, err
	}

	deletedLinkData, _, _, err := g.Connection.QueryNeoAll(deletedLinkQuery, nil)
	if err != nil {
		log.Errorf("could not execute link created Query: %v", err)
		return nil, nil, nil, nil, err
	}

	createdLinks := make([]KvisLink, 0)
	createdLinks = createLinks(dataLinks)

	deletedLinks := make([]KvisLink, 0)
	deletedLinks = createLinks(deletedLinkData)

	log.Debugf("Found total %d created Links", len(createdLinks))
	log.Debugf("Found total %d deleted Links", len(deletedLinks))

	return createdNodes, deletedNodes, createdLinks, deletedLinks, nil

}

func (g *Graph) GetNode(id string) (*KvisNode, error) {

	log.Debugf("Query for node is: %s", id)

	nodeQuery := fmt.Sprintf("MATCH (n) WHERE ID(n) = %s RETURN n", id)

	nodeData, _, _, err := g.Connection.QueryNeoAll(nodeQuery, nil)

	if err != nil {
		log.Errorf("could not execute node createdQuery: %v", err)
		return nil, err
	}

	createdNodes := make([]KvisNode, 0)
	createdNodes = getNodes(nodeData)

	if len(createdNodes) < 1 {
		log.Error("No node found")
		return nil, errors.New("no node found")
	}

	return &createdNodes[0], nil

}
