package controller

import (
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	logger "gitlab.com/hexhibit/kviz/logger"
	v12 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/apimachinery/pkg/watch"
	"gitlab.com/hexhibit/kviz/graph"
	"time"
	"net"
	"os"
	"fmt"
	"github.com/go-errors/errors"
	"encoding/json"
)

var (
	log   = logger.Logger()
	Nodes = make(map[string]*v1.Node)
	Pods  = make(map[string]*v1.Pod)
)

type Controller struct {
	Events       []time.Time
	EventChannel chan time.Time
	Api          v12.CoreV1Interface
}

func (c *Controller) Run(clientSet *kubernetes.Clientset) {

	log.Infof("creating controller")

	host := os.Getenv("NEO4J_HOST")
	port := os.Getenv("NEO4J_PORT")
	waitForConnection(host, port)

	c.EventChannel = make(chan time.Time)

	graph.InitGraphDb()

	graph, err := graph.GetGraph()
	if err != nil {
		panic(err)
	}

	graph.ClearDatabase()

	c.Events = make([]time.Time, 0)
	c.Api = clientSet.CoreV1()
	c.watchResources()

}

func waitForConnection(host string, port string) {

	timeout := time.After(60 * time.Second)
	tick := time.Tick(1 * time.Second)

	for {
		select {
		case <-timeout:
			panic(fmt.Sprintf("could not reach neo4j database on %s:%s", host, port))
		case <-tick:
			conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), 5*time.Second)
			if conn != nil && err == nil {
				conn.Close()
				log.Infof("database seems reachable")
				return
			}
			log.Infof("wait for connection on %s:%s .......", host, port)
		default:

		}
	}
}

func (c *Controller) watchResources() {
	go c.watchNodes()
	go c.watchPods()
}

//Starts with an delay of 5 sec. to ensure all node events are catched and processed beforehand
func (c *Controller) watchPods() {

	time.Sleep(5 * time.Second)

	log.Info("start pod watcher")

	ns := os.Getenv("WATCH_NAMESPACE")
	/*if ns == "" {
		ns = "default"
	}*/

	log.Infof("create pod watcher for namespace %s", ns)

	watcher, err := c.Api.Pods(ns).Watch(metav1.ListOptions{})
	if err != nil {
		log.Errorf("could not create pod watcher: %v", err)
		return
	}

	ch := watcher.ResultChan()
	for event := range ch {

		graph, err := graph.GetGraph()
		if err != nil {
			log.Errorf("could not open db connection for pd watcher: %v", err)
			continue
		}

		pod, ok := event.Object.(*v1.Pod)
		if !ok {
			log.Errorf("unexpected type, expected v1.Pod")
			continue
		}
		podId := pod.Namespace + "-" + pod.Name
		switch event.Type {
		case watch.Added:
			log.Debugf("new Pod added: name: %s namespace: %s created: %s", pod.Name, pod.Namespace, pod.CreationTimestamp)
			Pods[podId] = pod
			graph.CreatePods([]*v1.Pod{pod})
			node, ok := Nodes[pod.Spec.NodeName]
			if ok {
				graph.CreatePodToNodeRel([]*v1.Node{node}, []*v1.Pod{pod})
			} else {
				log.Errorf("no node found with name %s, event timestamp: %s", pod.Spec.NodeName, pod.CreationTimestamp)
			}
			timestamp := pod.CreationTimestamp.Time
			c.Events = append(c.Events, timestamp)

			//non blocking send
			select {
			case c.EventChannel <- timestamp:
			default:

			}
		case watch.Deleted:
			log.Debugf("new Pod deleted: name: %s namespace: %s created: %s", pod.Name, pod.Namespace, pod.CreationTimestamp)
			graph.DeletePod(pod)
			delete(Pods, podId)
			timestamp := pod.DeletionTimestamp.Time
			c.Events = append(c.Events, timestamp)

			//non blocking send
			select {
			case c.EventChannel <- timestamp:
			default:

			}

		}
	}
}

func (c *Controller) watchNodes() {

	log.Info("start node watcher")

	watcher, err := c.Api.Nodes().Watch(metav1.ListOptions{})
	if err != nil {
		log.Errorf("could not create node watcher: %v", err)
		return
	}

	ch := watcher.ResultChan()
	for event := range ch {

		graph, err := graph.GetGraph()
		if err != nil {
			log.Errorf("could not open db connection for pd watcher: %v", err)
			continue
		}

		node, ok := event.Object.(*v1.Node)
		if !ok {
			log.Errorf("unexpected type, expected v1.Node")
			continue
		}
		nodeId := node.Name
		switch event.Type {
		case watch.Added:
			log.Debugf("new Node added: name: %s namespace: %s created: %s", node.Name, node.Namespace, node.CreationTimestamp, )
			graph.CreateNodes([]*v1.Node{node})
			graph.CreateNodeToNodeRel(node, Nodes)
			Nodes[nodeId] = node
			timestamp := node.CreationTimestamp.Time
			c.Events = append(c.Events, timestamp)

			//non blocking send
			select {
			case c.EventChannel <- timestamp:
			default:

			}
		case watch.Deleted:
			log.Debugf("new Node deleted: name: %s namespace: %s created: %s", node.Name, node.Namespace, node.CreationTimestamp, )
			delete(Nodes, nodeId)
			timestamp := node.DeletionTimestamp.Time
			c.Events = append(c.Events, timestamp)

			//non blocking send
			select {
			case c.EventChannel <- timestamp:
			default:

			}
		}

	}
}

func (c *Controller) GetNodeInfo(id string) ([]byte, error) {

	var data []byte
	var err error

	graph, err := graph.GetGraph()
	if err != nil {
		return nil, err
	}

	kvizNode, err := graph.GetNode(id)
	if err != nil {
		return nil, err
	}

	switch kvizNode.NodeGroup {
	case "Node":
		//Node
		n := &v1.Node{}
		n, err = c.getNode(kvizNode.Name)
		if err == nil {
			data, err = json.Marshal(n)
		}
	case "Pod":
		//Pod
		p := &v1.Pod{}
		p, err = c.getPod(kvizNode.Name, kvizNode.Namespace)
		if err == nil {
			data, err = json.Marshal(p)
		}
	case "Container":
		//Container
	default:

	}

	if err != nil {
		return nil, err
	}

	return data, nil

}

func (c *Controller) getNode(name string) (*v1.Node, error) {
	return c.Api.Nodes().Get(name, metav1.GetOptions{})
}

func (c *Controller) getPod(name string, namespace string) (*v1.Pod, error) {
	return c.Api.Pods(namespace).Get(name, metav1.GetOptions{})
}

func (c *Controller) getContainer(podName string, namespace string, containerName string) (*v1.Container, error) {
	pod, err := c.getPod(podName, namespace)
	if err != nil {
		return nil, err
	}

	for _, container := range pod.Spec.Containers {
		if container.Name == containerName {
			return &container, nil
		}
	}

	return nil, errors.New(fmt.Sprintf("No container found on pod %s in namespace %s with name %s", podName, namespace, containerName))
}

func (c *Controller) ListGraph(time time.Time) ([]byte, error) {

	graph, err := graph.GetGraph()
	if err != nil {
		return nil, err
	}

	nodes, links, err := graph.List(time)
	if err != nil {
		return nil, err
	}

	response := Response{
		Nodes: nodes,
		Links: links,
	}

	d, err := json.Marshal(&response)
	if err != nil {
		return nil, err
	}
	return d, nil

}

type Response struct {
	Nodes []graph.KvisNode `json:"nodes""`
	Links []graph.KvisLink `json:"links"`
}

func (c *Controller) GetDelta(from time.Time, to time.Time) ([]byte, error) {

	graph, err := graph.GetGraph()
	if err != nil {
		return nil, err
	}

	createdNodes, deletedNodes, createdLinks, deletedLinks, err := graph.ListDelta(from, to)

	if err != nil {
		return nil, err
	}

	deltaResponse := struct {
		Added   Response `json:"added"`
		Removed Response `json:"removed"`
	}{
		Added: Response{
			Nodes: createdNodes,
			Links: createdLinks,
		},
		Removed: Response{
			Nodes: deletedNodes,
			Links: deletedLinks,
		},
	}

	d, err := json.Marshal(&deltaResponse)
	if err != nil {
		return nil, err
	}
	return d, nil

}
