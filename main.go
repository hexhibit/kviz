package main

import (
	"path/filepath"
	"os"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/kubernetes"

	"runtime"
	"gitlab.com/hexhibit/kviz/controller"
	"github.com/op/go-logging"
	"fmt"
	logger "gitlab.com/hexhibit/kviz/logger"
	"gitlab.com/hexhibit/kviz/server"
	"k8s.io/client-go/rest"
	"github.com/common-nighthawk/go-figure"
)

func main() {

	figure.NewFigure("kviz", "doom", true).Print()
	fmt.Print("\n\n\n\n")

	logger.ConfigureLogger(logging.INFO)

	con := &controller.Controller{}

	inCluster := os.Getenv("IN_CLUSTER")

	var clientSet *kubernetes.Clientset
	if inCluster == "true" {
		clientSet = fromInsideCluster()
	} else {
		clientSet = fromOutsideCluster()
	}

	con.Run(clientSet)

	server.Run(con)

	//Block >:-O
	fmt.Scanln()

}

func fromInsideCluster() (*kubernetes.Clientset) {
	logger.Logger().Debugf("Load cluster config")
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
		panic(fmt.Sprintf("could not read incluster config from, reason: %v", err))
	}
	// creates the client set
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(fmt.Sprintf("could not read incluster config from, reason: %v", err))
	}

	return clientSet
}

func fromOutsideCluster() (*kubernetes.Clientset) {
	logger.Logger().Debugf("Load local config")
	homeDir := ""
	//TODO cluster context
	if runtime.GOOS == "windows" {
		homeDir = os.Getenv("UserProfile")
	} else {
		homeDir = os.Getenv("HOME")
	}

	kubeConfig := filepath.Join(homeDir, ".kube", "config", )

	kubeFilePath := os.Getenv("KUBE_FILE_PATH")
	if kubeFilePath != "" {
		kubeConfig = filepath.Join(kubeFilePath, "config", )
	}

	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	if err != nil {
		panic(fmt.Sprintf("could not read config from %s, reason: %v", kubeFilePath, err))
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(fmt.Sprintf("could not create clientset from %s, reason: %v", kubeFilePath, err))
	}
	return clientSet
}
