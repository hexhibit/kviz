FROM golang:1.10.3-alpine AS build-stage

ARG GITLAB_TOKEN_ARG

MAINTAINER christian@schlatter.pw
WORKDIR /go/src/
COPY ./ gitlab.com/hexhibit/kviz/
RUN apk add --update --no-cache build-base\
    && apk add git

RUN cd gitlab.com/hexhibit/kviz \
    && pwd \
    && ls -la\
    && go get -v ./...\
    && ls /go/src\
    && CGO_ENABLED=1 GOOS=`go env GOHOSTOS` GOARCH=`go env GOHOSTARCH` go build -v -o kviz


# fetch gui
FROM hub.xeha.ch/hexhibit/kviz-gui As gui

# production stage
FROM alpine:3.5
MAINTAINER christian@schlatter.pw

#Copy binary
COPY --from=build-stage /go/src/gitlab.com/hexhibit/kviz/kviz .

#Copy statics
# COPY ./static ./static
COPY --from=gui /usr/share/nginx/html ./static

#Add CA certificates
RUN apk --update add bash ca-certificates

ENTRYPOINT ./kviz